<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    protected $user;


    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->guard()->user();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->user->products()->get(['id', 'name', 'price', 'quantity', 'created_by']);
    
        return response()->json($products->toArray());

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'       => 'required|string',
                'price'      => 'required',
                'quantity'   => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }

        $product            = new Product();
        $product->name     = $request->name;
        $product->price      = $request->price;
        $product->quantity = $request->quantity;

        if ($this->user->products()->save($product)) {
            return response()->json(
                [
                    'status'    => true,
                    'product'   => $product,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Product could not be saved.',
                ]
            );
        }

    }


      /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->user->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return $product;

    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product         $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = $this->user->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product with id ' . $id . ' cannot be found'
            ], 400);
        }
        
        $validator = Validator::make(
            $request->all(),
            [
                'name'       => 'required|string',
                'price'      => 'required',
                'quantity'   => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }
        $product->name       = $request->name;
        $product->price      = $request->price;
        $product->quantity   = $request->quantity;

        if ($this->user->products()->save($product)) {
            return response()->json(
                [
                    'status' => true,
                    'product'   => $product,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Product could not be updated.',
                ]
            );
        }

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->user->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, product with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        if ($product->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Product deleted'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Product could not be deleted'
            ], 500);
        }

    }

    protected function guard()
    {
        return Auth::guard();

    }








}
